#include <SDL2/SDL.h> 
#include <stdio.h> 
#include <time.h>
#include <stdlib.h>

//------------------------------------------------------------------------------
// Enum for game states
//------------------------------------------------------------------------------
enum GameState
{
	STATE_WAIT,
	STATE_PLAY,
	STATE_DEAD,
};

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------
const int 		SCREEN_WIDTH 	= 640;
const int 		SCREEN_HEIGHT 	= 480;
const int		SPEED_GRAVITY	= 4;
const int		SPEED_FLY		= 5;
const int		FLY_UPDATES		= 10;
const int		TICKS_INTERVAL	= 16;
const int		BALL_X			= 100;
const int		BALL_Y			= SCREEN_HEIGHT/2;;
const int		PIPE_COUNT		= 5;
const int		PIPE_START_X	= 600;
const int		PIPE_SPACE_X	= 300;
const int		PIPE_SPACE_Y	= 150;
const int		PIPE_SPEED		= 2;
const int		PIPE_VAR_Y		= 200;
const int		PIPE_MID_Y		= (SCREEN_HEIGHT/2) + (PIPE_SPACE_Y/2);

//------------------------------------------------------------------------------
// Global variables
//------------------------------------------------------------------------------
SDL_Window* 	gWindow 		= NULL;
SDL_Surface* 	gSurfScreen 	= NULL;
SDL_Surface* 	gSurfBall 		= NULL;
SDL_Surface* 	gSurfPipeUp 	= NULL;
SDL_Surface* 	gSurfPipeDown 	= NULL;

SDL_Rect 		gRectScreen;
SDL_Rect 		gRectBall;

int				gBallUpdateFly	= 0;
int				gNextUpdate		= 0;

int				gPipeX[PIPE_COUNT] = {0};
int				gPipeY[PIPE_COUNT] = {0};

int				gDeadUpdateCounter	= 60;

bool			gKeySpace = false;
bool			gBallIsVisible = true;

enum GameState	gState = STATE_WAIT;

//------------------------------------------------------------------------------
// Forward declaration
//------------------------------------------------------------------------------
bool 			init();
bool 			mainLoop();
int 			getRandomPipeY();
void 			initRects();
void 			cleanup();
void 			blitPipes();
SDL_Surface* 	loadImage(const char* path);
unsigned int 	getTicksLeft();

void 			updateStateWait();
void 			updateStatePlay();
void 			updateStateDead();

void			onWait();
void			onPlay();
void			onDead();

//------------------------------------------------------------------------------
// Main
//------------------------------------------------------------------------------
int main( int argc, char* args[] )
{
	if (init() == false)
		return 1;

	mainLoop();
	cleanup();
	return 0; 
} 

//------------------------------------------------------------------------------
// Entry points
//------------------------------------------------------------------------------
bool init()
{
	srand(time(NULL));

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
		return false;
	}
	else 
	{ 
		//Create window 
		gWindow = SDL_CreateWindow(	"Flappy Ball",
									SDL_WINDOWPOS_UNDEFINED, 
									SDL_WINDOWPOS_UNDEFINED, 
									SCREEN_WIDTH, 
									SCREEN_HEIGHT, 
									SDL_WINDOW_SHOWN ); 
		
		if( gWindow == NULL ) 
		{ 
			printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() ); 
			return false;
		}
		else 
		{ 
			gSurfScreen = SDL_GetWindowSurface( gWindow );
		} 
	}
	
	gSurfBall 		= loadImage("ball.bmp");
	gSurfPipeUp 	= loadImage("pipe_up.bmp");
	gSurfPipeDown 	= loadImage("pipe_down.bmp");
	return true;
}

bool mainLoop()
{
	bool quit = false;
	initRects();
	gNextUpdate = SDL_GetTicks() + TICKS_INTERVAL;

	//While application is running
	while (!quit)
	{
		gKeySpace = false;

		//Handle events on queue
		SDL_Event e;
		while (SDL_PollEvent( &e ) != 0)
		{
			switch (e.type)
			{
				case SDL_QUIT:
					quit = true;
					break;

				case SDL_KEYDOWN:
					if (e.key.keysym.sym == SDLK_SPACE)
						gKeySpace = true;
					break;
			}
		}

		if (quit) break;

		switch (gState)
		{
			case STATE_WAIT: updateStateWait(); break;
			case STATE_PLAY: updateStatePlay(); break;
			case STATE_DEAD: updateStateDead(); break;
		}

		// Clear screen, Draw the objects, Update the screen
		SDL_FillRect(gSurfScreen, &gRectScreen, 0xffffffff);

		if (gBallIsVisible)
			SDL_BlitSurface(gSurfBall, NULL, gSurfScreen, &gRectBall );

		blitPipes();
		SDL_UpdateWindowSurface(gWindow);

		// Wait some time before going through the loop again
		SDL_Delay(getTicksLeft()); 
		gNextUpdate += TICKS_INTERVAL;
	}

	return false;
}

void cleanup()
{
    //Deallocate surfaces
    SDL_FreeSurface( gSurfBall );
    gSurfBall = NULL;

    SDL_FreeSurface( gSurfPipeUp );
    gSurfPipeUp = NULL;

    SDL_FreeSurface( gSurfPipeDown );
    gSurfPipeDown = NULL;

    SDL_FreeSurface( gSurfScreen );
    gSurfScreen = NULL;

	//Destroy window 
	SDL_DestroyWindow( gWindow ); 
	gWindow = NULL;

	//Quit SDL subsystems 
	SDL_Quit(); 
}

void blitPipes()
{
	SDL_Rect rectPipeUp;
	SDL_Rect rectPipeDown;

	rectPipeUp.x = 0;
	rectPipeUp.y = 0;
	rectPipeUp.w = gSurfPipeUp->w;
	rectPipeUp.h = gSurfPipeUp->h;

	rectPipeDown.x = 0;
	rectPipeDown.y = 0;
	rectPipeDown.w = gSurfPipeDown->w;
	rectPipeDown.h = gSurfPipeDown->h;

	for (int i=0; i<PIPE_COUNT; i++)
	{
		rectPipeUp.x = gPipeX[i];
		rectPipeUp.y = gPipeY[i] - PIPE_SPACE_Y - gSurfPipeUp->h;

		rectPipeDown.x = gPipeX[i];
		rectPipeDown.y = gPipeY[i];

		// Draw pipes
		SDL_BlitSurface(gSurfPipeUp, NULL, gSurfScreen, &rectPipeUp);
		SDL_BlitSurface(gSurfPipeDown, NULL, gSurfScreen, &rectPipeDown);
	}
}

//------------------------------------------------------------------------------
// States
//------------------------------------------------------------------------------

void onWait()
{
	initRects();
	gRectBall.x = BALL_X;
	gRectBall.y = BALL_Y;
	gState = STATE_WAIT;
}

void updateStateWait()
{
	if (gKeySpace)
		onPlay();
}

void onPlay()
{
	gState = STATE_PLAY;
}

void updateStatePlay()
{
	if (gKeySpace)
		gBallUpdateFly = FLY_UPDATES;

	if (gBallUpdateFly > 0)
	{
		gBallUpdateFly--;
		gRectBall.y -= SPEED_FLY;

		if (gRectBall.y < 0)
			gRectBall.y = 0;
	}
	else
	{
		gRectBall.y += SPEED_GRAVITY;
		if (gRectBall.y + gRectBall.h > SCREEN_HEIGHT)
			gRectBall.y = SCREEN_HEIGHT - gRectBall.h;
	}

	SDL_Rect rectPipeUp;
	SDL_Rect rectPipeDown;

	rectPipeUp.x = 0;
	rectPipeUp.y = 0;
	rectPipeUp.w = gSurfPipeUp->w;
	rectPipeUp.h = gSurfPipeUp->h;

	rectPipeDown.x = 0;
	rectPipeDown.y = 0;
	rectPipeDown.w = gSurfPipeDown->w;
	rectPipeDown.h = gSurfPipeDown->h;

	for (int i=0; i<PIPE_COUNT; i++)
	{
		gPipeX[i] -= PIPE_SPEED;

		rectPipeUp.x = gPipeX[i];
		rectPipeUp.y = gPipeY[i] - PIPE_SPACE_Y - gSurfPipeUp->h;

		rectPipeDown.x = gPipeX[i];
		rectPipeDown.y = gPipeY[i];

		// Check if the ball hit one of the pipes
		if (SDL_HasIntersection(&gRectBall, &rectPipeUp) ||
			SDL_HasIntersection(&gRectBall, &rectPipeDown))
		{
			onDead();
		}

		// move to the right side if pipe is < 0 x
		if (gPipeX[i] + gSurfPipeUp->w < 0)
		{
			int idxPrev = i - 1;
			while (idxPrev < 0)
				idxPrev += PIPE_COUNT;

			gPipeX[i] = gPipeX[idxPrev] + PIPE_SPACE_X;
			gPipeY[i] = getRandomPipeY();
		}
	}
}

void onDead()
{
	gState = STATE_DEAD;
	gDeadUpdateCounter = 60;
}

void updateStateDead()
{
	gDeadUpdateCounter--;
	if (gDeadUpdateCounter <= 0)
	{
		onWait();
		gBallIsVisible = true;
	}

	// 5 frames on, 5 frames off
	if ((gDeadUpdateCounter / 5) % 2 == 0)
		gBallIsVisible = true;
	else
		gBallIsVisible = false;
}

//------------------------------------------------------------------------------
// Helper functions
//------------------------------------------------------------------------------

SDL_Surface* loadImage(const char* path)
{
	SDL_Surface* s = SDL_LoadBMP(path);

	// Set magenta as color key ff00ff
	Uint32 colorkey = SDL_MapRGB( s->format, 0xFF, 0x00, 0xFF );
	SDL_SetColorKey( s, SDL_TRUE, colorkey );
	return s;
}

void initRects()
{
	gRectScreen.x = 0;
	gRectScreen.y = 0;
	gRectScreen.w = SCREEN_WIDTH;
	gRectScreen.h = SCREEN_HEIGHT;

	gRectBall.x = BALL_X;
	gRectBall.y = BALL_Y;
	gRectBall.w = gSurfBall->w;
	gRectBall.h = gSurfBall->h;

	for (int i=0; i<PIPE_COUNT; i++)
	{
		gPipeX[i] = PIPE_START_X + (i * PIPE_SPACE_X);
		gPipeY[i] = getRandomPipeY();
	}
}

int getRandomPipeY()
{
	// say PIPE_VAR_Y = 200, y can be +/-100
	int y = (rand() % PIPE_VAR_Y) - (PIPE_VAR_Y/2);
	return y + PIPE_MID_Y;
}

unsigned int getTicksLeft()
{
    unsigned int now = SDL_GetTicks();
    if (gNextUpdate <= now)
        return 0;
    else
        return gNextUpdate - now;
}

